"use strict";

document.addEventListener('DOMContentLoaded', () => {

    // запрос на получение инфы и пользователях с GitHub (их имен) для составления запроса по каждому из них
    async function getUsers() {
        const users = await fetch('https://api.github.com/users');

        let result;

        if (users.status === 200) result = await users.json();

        // for (let key in result) {
        //     getUser(result[key].login);
        // }

        result.forEach((user) => getUser(user.login));
    }

    //запрос на каждого пользователя дабы получить более полную информацию о каждом
    async function getUser(nameCurrentUser) {
        const user = await fetch(`https://api.github.com/users/${nameCurrentUser}`);
        let result = null;

        if (user.status === 200) result = await user.json();

        userRenderingOnPage(result);
    }

    //отрисовка таблицы с данными пользователей
    const userRenderingOnPage = (user) => {

        const tablePage = document.querySelector('.main__usersGithub');

        const tr = document.createElement('tr');

        tr.innerHTML = `<td>${user['login']}</td>
                        <td>${user['id']}</td>
                        <td>${user['name'] ? user['name'] : ''}</td>
                        <td>${user['location'] ? user['location'] : ''}</td>
                        <td>${user['node_id']}</td>
                        <td>${user['created_at']}</td>
                        <td><img src="${user['avatar_url']}" alt="avatar"></td>
                        <td>${user['url']}</td>
                        <td>${user['type']}</td>
                        <td>${user['followers']}</td>
                        <td>${user['public_repos']}</td>`;

        tablePage.append(tr);
    };

    getUsers();
});
